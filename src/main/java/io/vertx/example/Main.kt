package io.vertx.example

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.provider
import io.vertx.example.foundation.KExpress
import io.vertx.example.foundation.KExpress.Companion.vertx
import io.vertx.example.middlewares.BasicAuth
import io.vertx.example.middlewares.Logging
import io.vertx.example.repositories.UserRepository
import io.vertx.example.repositories.UserRepositoryImpl
import io.vertx.example.routes.IndexRouter
import io.vertx.example.routes.UserRouter
import io.vertx.example.services.User
import io.vertx.example.services.UserService
import io.vertx.example.services.UserServiceImpl
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.impl.TemplateHandlerImpl
import io.vertx.ext.web.templ.PebbleTemplateEngine
import java.util.*
import io.vertx.example.database.connection
import io.vertx.example.routes.i18nRouter

val Injection = Kodein {
    val userModule = Kodein.Module {
        bind<UserService>() with provider { UserServiceImpl() }
        bind<UserRepository>() with provider { UserRepositoryImpl() }
    }

    import(userModule)
}

fun main(args: Array<String>) {
    val app = KExpress()
    var test = connection()
    test.connect()
    //app.use(Logging())

    //app.use(BasicAuth())

   var array = arrayListOf<String>("/en","/tw","/cn")
   for (path in array) {
        app.use(path +"/" ,IndexRouter())
        app.use(path + "/users", UserRouter())
   }
    app.use("/" ,i18nRouter())
    app.listen(8080)
}

