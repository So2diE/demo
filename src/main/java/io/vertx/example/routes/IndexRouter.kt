package io.vertx.example.routes

import io.vertx.example.foundation.KRouter
import io.vertx.example.services.User
import io.vertx.ext.web.LanguageHeader
import io.vertx.ext.web.handler.RedirectAuthHandler
import io.vertx.ext.web.impl.ParsableLanguageValue
import kotlinx.coroutines.experimental.delay
import java.net.URL
import java.util.*
import javax.xml.ws.RequestWrapper
interface getLang {
    fun getLangeuage(url: String):String{
        var url = url
        val rootAddress:String = "http://localhost:8080/"
        url = url.replace(rootAddress,"/").substring(1,3)
        when(url){
            "tw"-> url = "zh-tw"
            "cn" -> url = "zh-cn"
            else -> url = "en-us"
        }
        return url
    }
}

class IndexRouter : KRouter(),getLang {
    init {

        get("/index.html").renderHTML("/templates/index.peb") {
            context->

            var url:String = getLangeuage(context.request().absoluteURI().toString())
            context.data()["user"] = User(id = 2, name = "Gary")
            context.acceptableLanguages().set(0, ParsableLanguageValue(url))
        }

        get("/").handleCoroutine { req, res ->
                res.end("O")
        }
        get("/user").handleCoroutine { req, res ->

            res.end("OK")
        }
        get("/5ms").handleCoroutine { req, res ->
            delay(5)
            res.end("OK")
        }
        get("/10ms").handleCoroutine { req, res ->
            delay(10)
            res.end("OK")
        }
    }
}
