package io.vertx.example.routes

import io.vertx.core.http.HttpMethod
import io.vertx.example.foundation.KRouter
import io.vertx.example.services.User
import io.vertx.ext.web.impl.ParsableLanguageValue
import io.vertx.reactivex.ext.web.RoutingContext
import kotlinx.coroutines.experimental.delay
import java.util.*


class i18nRouter : KRouter() {
    init {
    val method = arrayListOf<HttpMethod>(HttpMethod.GET,
            HttpMethod.POST,HttpMethod.DELETE,HttpMethod.PUT)
      for (m in method) {
          route("/*").method(m).handler({ RoutingContext ->
              var locale = Locale.getDefault()
            var url = RoutingContext.request().absoluteURI()
              val rootAddress:String = "http://localhost:8080/"
              url = url.replace(rootAddress,"/")
              var redirect: String = "en"
              when (locale.toString()) {
                  "zh-CN" -> redirect = "cn"
                  "zh-TW" ,"zh-HK" ,"zh-SG" ,"zh-MO" -> redirect = "tw"
                  else -> redirect = "en"
              }

              var path = "/" + locale.language + url
              RoutingContext.reroute(m ,path)
          })
      }

}}

